# README #

Welcome to the (messy) source code of [@tamamoebot](https://twitter.com/tamamoebot) image bot on twitter :)

Here are all the different parts of @tamamoebot, from the twitter poster script to the scripts used to display information on the website.

Written in python, with the help of a lot of modules already pre-written.

For now, it's more a "save point" for me before engaging in a loong refactoring over the summer, to make the code ever more clean and fast (... I hope !). All the parts should work (with the right modules installed), detailed installation instructions will come in a later commit.

In the meantime, pull requests are more than welcome :3

If you find any bug, as insignificant as it is, feel free to [open an issue](https://bitbucket.org/tamati25/moe-bot/issues/new) or send me a tweet [@tama_92](https://www.twitter.com/tama_92), thanks !