#!/usr/bin/env python

'''
The MIT License (MIT)
Copyright (c) 2013 Dave P.
'''

import os
import signal, sys, ssl
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer, SimpleSSLWebSocketServer
from optparse import OptionParser

import loader
import uuid
import ipgetter
import ConfigParser

import re

from datetime import datetime

authorized = ['127.0.0.1']

cfg = ConfigParser.ConfigParser()
cfg.read(os.path.dirname(os.path.realpath(__file__)) + os.sep + "MoeWSServer.py.config")
plugins_folder = cfg.get("general", "plugins_folder")

class MoeHandler(WebSocket):  
   clients = {}
   plugins = {}
   msg_re = re.compile("[^/]+/[^/]+/.+")

   @staticmethod
   def getPlugins():
      return MoeHandler.plugins

   @staticmethod
   def setPlugins(plugins):
      print hex(id(MoeHandler.plugins))
      MoeHandler.plugins = plugins

   def on_hotswap(self):
      print '[1]'
      for (k,v) in self.getPlugins().items():
         v().on_hotswap()

   def isLocal(self):
      return (self.address[0] in authorized)

   def dispatch(self, message):
#      print '>>> MoeHandler.Plugins loaded :'
#      print '\033[1;31m', hex(id(self.getPlugins())), "\033[0m : ", self.getPlugins()
      print '[ ' + str(datetime.now()) + ' < ' + str(self.sid) + ' @ ' + str(self.address[0]) + ' ] ' + str(message)

      m = re.match(self.msg_re, str(message))
      if m is None:
         print 'Invalid message format'
         self.sendMessage('status/400')
         return

      sid = message.split('/')[0]
      target = message.split('/')[1]
      data = '/'.join(message.split('/')[2:])

      if not sid in MoeHandler.clients:
         self.sendMessage('status/401')
         return

      if not target in self.getPlugins():
         self.sendMessage('status/404')
         return

      handler = None
      if not target in MoeHandler.clients[self.sid]:
         MoeHandler.clients[self.sid][target] = (self.getPlugins()[target](), False)

      handler = MoeHandler.clients[self.sid][target][0]
      isPrivate = handler.isPrivate()

      if handler is None:
         self.sendMessage('status/404')
         return

      # Delegate the message handling to the right plugin
      if data == 'connect':
         if target in MoeHandler.clients[self.sid] and MoeHandler.clients[self.sid][target][1] is True:
            self.sendMessage('status/400')
            return
         connected = handler.on_connect(self)
         if connected:
            MoeHandler.clients[self.sid][target] = (handler, True)
            self.sendMessage('status/200')
      elif data == 'disconnect':
         if not target in MoeHandler.clients[self.sid]:
            self.sendMessage('status/400')
            return
         handler.on_disconnect(self)
         MoeHandler.clients[self.sid][target] = (handler, False)
         self.sendMessage('status/200')
      else:
         if MoeHandler.clients[self.sid][target][1] is False:
            #Not connected.
            self.sendMessage('status/401')
            return

         if (isPrivate is False or self.isLocal() is True):
            handler.on_message(self, data)
         else:
            self.sendMessage('status/403')
            return

            self.sendMessage('status/200')

   def handleMessage(self):
      self.dispatch(self.data)

   def handleConnected(self):
      self.sid = str(uuid.uuid4())

      MoeHandler.clients[self.sid] = {}
      self.sendMessage(u"sid/" + self.sid)
      print MoeHandler.clients

   def handleClose(self):
      for p in MoeHandler.clients[self.sid]:
         self.dispatch(self.sid + "/" + p + "/disconnect")
      MoeHandler.clients.pop(self.sid)

#def reload_plugins():
#   global plugins

#   print "MoeWSServer @ ", hex(id(sys.modules[__name__]))
#   print '[1]'

#   plugins = loader.get_plugins(plugins_folder)
#   print hex(id(plugins)), ": ", plugins


if __name__ == "__main__":
   MoeHandler.plugins = loader.get_plugins(plugins_folder)
   cls = MoeHandler
   authorized.append(ipgetter.myip())
   server = SimpleWebSocketServer('', 8001, cls)

   def close_sig_handler(signal, frame):
      server.close()
      sys.exit()

   signal.signal(signal.SIGINT, close_sig_handler)

   print 'Server started'
   server.serveforever()
