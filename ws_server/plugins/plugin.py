#!/usr/bin/env python

from datetime import datetime

class Plugin(object):
        # Plugin name
        name = 'Interface'
        clients = []

        # If true, only messages from local ip will be
        # handled
        private = True

        def __init__(self):
                self.clients = []

	def getName(self):
		return self.name
		
        
        # Event handling, each plugin will need to
        # implement these
        def on_connect(self, s):
                raise Exception('Subclass must implement on_connect')

	def on_message(self, c, msg):
		raise Exception('Subclass must implement on_message')

        def on_disconnect(self, s):
                raise Exception('Subclass must implement on_disconnect')

        # Hotswapping, broadcast a warning message by default
        def on_hotswap(self):
                pass

        # Get informations
        def getClients(self):
                raise Exception('Subclass must implement getClients')

        def isPrivate(self):
                return self.private

# util (static) methods
debug = True
def send(client, message):
        if debug is True:
                print '[ ' + str(datetime.now()) + ' > ' + str(client.sid) + ' @ ' + str(client.address[0]) + ' ] ' + str(message)
        client.sendMessage(u'' + str(message))

def broadcast(clients, message):
        if debug is True:
                print 'Broadcast'
        for client in list(clients):
                send(client, message)

def lbroadcast(clients, message):
        if debug is True:
                print 'Local broadcast'
        for client in list(clients):
                if client.isLocal():
                        send(client, message)
