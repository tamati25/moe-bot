#!/usr/bin/env python

import plugins.plugin as p
from plugins.plugin import Plugin

class Broadcast(Plugin):
    name = "chat";
    private = False
    clients = []

    def __init__(self):
        super(self.__class__, self).__init__()
        pass

    def on_connect(self, s):
        print '[B] connect'
        p.broadcast(self.getClients(), 'Connected : ' + str(s.sid) + '@' + str(s.address[0]))
        self.getClients().append(s)
        return True

    def on_disconnect(self, s):
        print '[B] Disconnect'
        p.broadcast(self.getClients(), 'Disconnected : ' + str(s.sid) + '@' + str(s.address[0]))
        self.getClients().remove(s)

    def on_message(self, c, msg):
        print '[B] Handle message ', msg
        p.broadcast(self.getClients(), str(c.sid) + ' : ' + msg)

    def on_hotswap(self):
        print '[B] hotswap'

    def getClients(self):
        return Broadcast.clients
