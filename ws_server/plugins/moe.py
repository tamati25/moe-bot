#!/usr/bin/env python

import plugins.plugin as p
from plugins.plugin import Plugin

class Moe(Plugin):
    name = "moe";
    private = True
    clients =[]

    def __init__(self):
        super(self.__class__, self).__init__()
        pass

    def on_connect(self, s):
        print '[M] connect'
        self.getClients().append(s)
        return True

    def on_disconnect(self, s):
        print '[M] Disconnect'
        self.getClients().remove(s)

    def on_message(self, c, msg):
        #Non-local messages were already filtered
        print '[M] Handle message ', msg
        if msg == 'refresh':
            print 'Refresh'
            p.broadcast(self.getClients(), u'moe/refresh_data')

    def on_hotswap(self):
        print '[M] Hotswap'

    def getClients(self):
        return Moe.clients
