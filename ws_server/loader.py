#!/usr/bin/env python

import os
import sys
from plugins.plugin import Plugin


def get_plugins(path):
    plugins = {}
    sys.path.insert(0, path);
    for f in os.listdir(path):
        fname, ext = os.path.splitext(f)
        if ext == '.py':
#            print '  python file detected'
            mod = __import__(fname)
    sys.path.pop(0)
    for p in Plugin.__subclasses__():
        tmp = p()
        print "Loaded plugin", tmp.getName()
        plugins[tmp.getName()] = p
    return plugins

if __name__ == '__main__':
    print get_plugins("./plugins")
