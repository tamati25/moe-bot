#!/usr/bin/env python

import config_parser as Parser
import request as Request
from collections import Counter

def get_top_requests(p, n):
    if p.get_bool("active") is False:
        return disabled_html

    top_requests = Request.get_requests(p)
    top = Counter(top_requests).most_common(n)
    out = "<table class=\"top_requests\">"

    last = -1
    idx = 1
    for request in top:
        if request[1][0] < last:
            idx = idx + 1
        out = out + "<tr"
        if idx == 1:
            out = out + " class=\"top\""
        out = out + "><td class=\"rank\" rowspan=\"2\">"
        if request[1][0] < last or last == -1:
            out = out + str(idx)
        out = out + "</td><td class=\"first_row\">" + request[0].title() + "</td><td class=\"first_row\">" + str(request[1][0]) + "</td></tr>"
        out = out + "<tr><td class=\"last second_row\" colspan=\"2\">Last request : " + request[1][1] + "</td></tr>"
        if idx > n:
            break
        last = request[1][0]

    out = out + "</table>"
    print out
    return out

def get_table_from_file(file):
    p = Parser.Parser(file)
    return get_top_requests(p, p.get_int("n_requests"))

def get_table(p):
    return get_top_requests(p, p.get_int("n_requests"))
