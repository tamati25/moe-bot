#!/usr/bin/env python

from time import time, gmtime, strftime
from collections import OrderedDict
from random import randint
import ConfigParser
import config_parser as Parser
import operator
import re
import os
import os.path
import sys

reload(sys)
sys.setdefaultencoding("utf-8")

#TODO : config file
C_FROM_A_RE = "(.*) \(FROM (.*)\)"
C_from_A_RE = "(.*) \(from (.*)\)"

cfgParser = ConfigParser.ConfigParser()
cfgParser.read(os.path.dirname(os.path.realpath(__file__)) + os.sep + "request.py.config")
imgFolder = cfgParser.get("general", "image_folder")
config_file = cfgParser.get("general", "config_file")

def check_exists(request):
    if len(request) == 0:
        return False

    req = parse(request)
    result = find_picture(req[0], req[1], True)
    if result is None:
        return False
    return True

def parse(request):
    result = ()
    cFromA = re.compile(C_FROM_A_RE)
    m = cFromA.match(request)
    if m:
        result = (m.groups()[0], m.groups()[1])
    else:
        # sigh...
        cfromA = re.compile(C_from_A_RE)
        m2 = cfromA.match(request)
        if m2:
            result = (m2.groups()[0], m2.groups()[1])
        else:
            result = ('', request)
    return result

def find_picture(char, source, exit_on_first = False):
    #build candidates list
    char = char.lower()
    candidates = []
    for file in os.listdir(imgFolder):
#        print file.lower()
        if len(char) > 0 and not char in file.lower():
            continue;
        if not source.lower() in file.lower():
            continue;
        candidates.append(file)
        if exit_on_first is True:
            return candidates

    if len(candidates) == 0:
        return None

    r = randint(0, len(candidates) - 1)
    return candidates[r]

def is_request_taimu(p):
    if p.get_bool("active") is False:
        return False
    print 'Checking time'
    time = p.get_list("timetable")
    current_time = strftime("%H:%M", gmtime())
    if current_time in time:
        return True
    else:
        return False

def get_requests(p, archive=False):
    requests_file = p.get_string("requests")
    request_life = p.get_int("request_life")
    cooldown = p.get_int("cooldown")
    current_timestamp = time() 

    count = {}
    last_account_request = {}

    #File format : <timestamp>:<account>:<str>
    print requests_file
    if not os.path.isfile(requests_file):
        return OrderedDict()

    requests = [line.strip() for line in open(requests_file)]
    for request in requests:
        try:
            timestamp = int(request.split(":")[0])
            account = request.split(":")[1]
            content = ":".join(request.split(":")[2:]).lower()
            print timestamp, " - ", account, " - ", content
            if current_timestamp - timestamp > request_life:
                print "Too old"
                continue;
            if last_account_request.has_key(account) and current_timestamp - last_account_request[account] < cooldown:
                print "Too near"
                continue;
            last_account_request[account] = timestamp
            request_time = strftime("%H:%M", gmtime(timestamp))
            if count.has_key(content):
                count[content] = (count[content][0] + 1, request_time)
            else:
                count[content] = (1, request_time)
        except ValueError:
            pass

    if archive is True:
        folder = "/".join(requests_file.split("/")[:-1])
        archive_name = folder + "/requests_" + strftime("%d%m%Y_%H%M", gmtime())
        os.rename(requests_file, archive_name)
    current_timestamp = time()

    return OrderedDict(sorted(count.items(), key=operator.itemgetter(1), reverse=True))

def check(force = False):
    print 'Request check'
    p = Parser.Parser(config_file)
    if force is False and is_request_taimu(p) is False:
        return None
    else:
        requests = get_requests(p, True)
        #print requests
        if len(requests.items()) > 0:
            top_requests = []

            top = requests.items()[0]
            top_score = top[1]

            index = 0
            while index < len(requests.items()) and requests.items()[index][1] == top_score:
                this_score = requests.items()[index][1]
                #print requests.items()[index][0], "-", this_score, "(", top_score, ")"
                top_requests.append(requests.items()[index])
                index = index + 1

            r = randint(0, len(top_requests) - 1)
            top = top_requests[r]

            #print top[0], " - ", str(top[1])
            requested = parse(top[0])
            #print "Requested \033[1;31m", requested[0], "\033[0m from \033[1;31m", requested[1], "\033[0m"
            return find_picture(requested[0], requested[1])
        else:
            return None

if __name__ == '__main__':
    p = Parser.Parser(config_file)
    print get_requests(p)
