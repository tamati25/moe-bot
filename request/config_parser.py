#!/usr/bin/env python

class Parser(object):
    def __init__(self, path):
        self.conf = self.read_configuration(path)

    def read_configuration(self, path):
        conf = {}
        lines = [line.strip() for line in open(path)]
        for line in lines:
            if len(line) > 0 and line[0] != '#':
                key = str(line.split('=')[0]).strip()
                value = str(line.split('=')[1]).strip()
                conf[key] = value
                # print key, " => ", value
        return conf

    def get_string(self, key):
        return self.conf[key]

    def get_int(self, key):
        return int(self.conf[key])

    def get_bool(self, key):
        return (self.get_int(key) == 1)

    def get_yesno(self, key, text_yes, text_no):
        if self.get_bool(key) is True:
            return text_yes
        else:
            return text_no

    def get_list(self, key):
        return self.get_string(key).split(";")

    def get_table(self, key, htmlclass):

        # Return a configuration key as a table
        # Each line is separated by ";" while each column is separated by ","
        # Example : a,1;b,2;c,3 will yield :
        # a    1
        # b    2
        # c    3
        value = self.get_string(key);
        if value is None:
            return ""

        lines = value.split(";")
        out = "<table class=\"" + htmlclass + "\">";
        for line in lines:
            out = out + "<tr>"
            cols = line.split(",")
            for col in cols:
                out = out + "<td>" + col + "</td>"
            out = out + "</tr>"
        out = out + "</table>"
        return out

