# Downloader

## Downloads latest upvoted posts from reddit for a given account, on whitelisted subreddits

# Requirements

* python 2.7
* praw (`pip install praw`)

# Installation

1. Download `moe_downloader.py` and put it in a folder
2. Download `moe_downloader.py.config` and put it in the same folder as `moe_downloader.py`
3. Edit the values in the configuration file downloaded at (2) :

  * General
    - *data_folder* : Folder that will contain data files (subreddits allowed, files already uploaded ...)
    - *tmp_folder* : Images will be downloaded in this folder before being moved to their final destination
    - *download_folder* : Final location for the images, all the images successfully downloaded will be there
    - *prefix* : Add a prefix to images downloaded
    - *fetch* : Number of posts to fetch, default 25
    - *file_ext* : Allowed files extension separated by semi-colons (;), files that do not fall within the list will not be downloaded

  * Reddit
    - *auth_file* : File with reddit credentials, it is strongly advised to put the rights correctly on this file (400 for example) as it contains sensitive data
    - *url* : where should the script fetch the links, default is http://api.reddit.com/user/[user]/liked
    - *subreddits* : Allowed subreddits separated by semi-colons (;), posts from other subreddits will be ignored    
4. (optional) set up a cron/scheduled task so that the latest posts will be downloaded automatically, for example every 10 minutes

# Notes

The script will not work in real-time, i.e you'll need to run it to download what you want, that's why it is a good idea to make it run automatically every X minutes.
  