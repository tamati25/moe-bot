#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Moe downloader: Upload upvoted posts on reddit to a dropbox folder
Uses praw and dropbox api

TODO :
  - Add saved images [0%] (High)
  - Put password/API keys... in a separate file [50%] (Medium)
  - Handle imgur albums [0%] (High)
  - Handle generic images [0%] (Low)
  - Automatically get image type [95%] (High)
  - Only take into account some subreddits, defined in a separate file [100%] (Medium)
  - Reduce dependencies to extern libraries [0%] (Low)
  - Organize code in functions [0%] (Very High !)
  - Make it more generic/portable (file name, folder ...) [0%] (Low)
  - Get environment variables (and read them from a file) [0%] (Low)
  - Choose where to upload files [0%] (Low)
  - Clean [0%] (Medium)
  - Reduce log size [0%] (Medium)
  - Add color to log [0%] (Low)
  - Generate report [100%] (Low)
"""

__author__ = "/u/tama_92"

import praw
import dropbox
import urllib2
import os
import sys
import magic
import ConfigParser
from datetime import datetime
from subprocess import call

# FORCE UTF-8 GRRRR
reload(sys)
sys.setdefaultencoding("utf-8")

def format_text(string):
  if isinstance(string, unicode):
    string = string.encode('utf-8').strip()
#  string = ''.join([x if ord(x) < 128 else '' for x in string])
#  string = string.strip()
  return string

def log(string):
  print str(datetime.now()), string

def log_error(string):
  print '\033[1;31m', str(datetime.now()), string, '\033[0m'

# don't forget the trailing slash !
if __name__ == '__main__':
  config = ConfigParser.ConfigParser()
  config_file = "moe_downloader.py.config"
  config.read(config_file)

  data_folder = config.get("general", "data_folder")
  tmp_folder = config.get("general", "tmp_folder")
  download_folder = config.get("general", "download_folder")
  prefix = config.get("general", "prefix")
  fetch = config.getint("general", "fetch")
  file_ext = config.get("general", "file_ext").split(";")
  subreddits = config.get("reddit", "subreddits").split(";")
  auth_file = config.get("reddit", "auth_file")
  reddit_url = config.get("reddit", "url")

  print 'Retrieving credentials...'
  credentials = [line.strip() for line in open(auth_file)]
  login = credentials[0]
  password = credentials[1]

  # fetch the 25 latest by default

  log("-----")
  log("Started at " + str(datetime.now()))
  log("-----")

  r = praw.Reddit('moe downloader by /u/tama_92')

  log("Connecting as " + login)
  r.login(login, password)
  log("Reddit OK")

  uploaded = open(data_folder + 'uploaded.txt', 'ab+')
  log("Retrieving already uplodaded files...")
  alreadyUploaded = [line.strip() for line in open(data_folder + 'uploaded.txt')]
  for i in xrange(len(alreadyUploaded)):
    alreadyUploaded[i] = format_text(alreadyUploaded[i])

  ignored = []
  if os.path.exists(data_folder + "ignored.txt"):
    ignored = [line.strip() for line in open(data_folder + 'ignored.txt')]

  log("Fetching [" + str(fetch) + "] from " + reddit_url)
  liked = r.get_content(reddit_url, limit = fetch)

  count_ok = 0
  count_failed = 0
  count_ignored = 0

  if len(sys.argv) > 1:
    fetch = int(sys.argv[1])
    if len(sys.argv) > 2:
      reddit_url = sys.argv[2]

  log("Subreddit whitelist : " + str(subreddits))
  for post in liked:
    try:
      log("Post subreddit : " + str(post.subreddit))

      if not str(post.subreddit) in subreddits:
        log("Ignoring post because it is not in the subreddits list")
        count_ignored = count_ignored + 1
        continue

      title = format_text(post.title)
      fullTitle = title.replace("/", "_")
      url = post.url.encode("utf-8")

      if url.startswith("http://imgur.com"):
        code = url.split("/")[-1]
        url = "http://imgur.com/download/" + code

      if url in ignored:
        log("Ignored")
        count_ignored = count_ignored + 1
        continue

      if fullTitle in alreadyUploaded or url in alreadyUploaded:
        log("Already uploaded")
        count_ignored = count_ignored + 1
        continue

       
      #Download the image
      try:
        log("Downloading " + url + " to " + fullTitle)
        response = urllib2.urlopen(url)
        with open(str(tmp_folder) + fullTitle, 'w') as f:
          f.write(response.read())
          f.close()
      except urllib2.URLError as e:
        log("Failed to download : " + str(e.code))
        count_failed = count_failed + 1
        continue


      # Determine image type
      log('  Determining file type')
      filetype = magic.from_file(str(tmp_folder) + fullTitle)
      if 'PNG' in filetype:
        filetype = 'png'
      elif 'JPEG' in filetype or 'JPG' in filetype:
        filetype = 'jpeg'

      if filetype is None or filetype not in file_ext:
        log("Filetype not recognized or not in authorized list")
        count_ignored = count_ignored + 1
        continue
      
      fullTitleInDropbox = fullTitle + "." + filetype
      log('Image type : ' + filetype)

      os.rename(str(tmp_folder) + fullTitle, str(download_folder) + prefix + fullTitleInDropbox)
      log("Finished")
      uploaded.write(format_text(fullTitle) + "\n")
      count_ok = count_ok + 1

    except Exception as e:
      log(str(e))
      log('Unhandled exception occured, skipping')
      count_failed = count_failed + 1

  # Do some cleaning and exit
  log('Cleaning...')
  uploaded.close()
  log('------')
  log('Ended at ' + str(datetime.now()))
  log('  Uploaded ' + str(count_ok))
  log('  Failed   ' + str(count_failed))
  log('  Ignored  ' + str(count_ignored))
  log('------')
