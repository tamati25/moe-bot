# Poster

## Post downloaded images from various sources on twitter and handles all the twitter-related code (commands)

# Requirements

- python 2.7
- tweepy (`pip install tweepy`)
- (optional) dropbox (`pip install dropbox`)
- (optional) pushbullet (`pip install pushbullet`)
- (optional) websocket (`pip install websocket`)

# Installation

1. Download `moe_poster.py`
2. Download `moe_poster.py.config` and put it in the same folder as `moe_poster.py`
3. Download `data/replace.txt`
4. Download all the files in the `plugins` folder
5. Get twitter keys from [http://apps.twitter.com] and put them in a file, one string per line

  - TWITTER_API_KEY
  - TWITTER_API_SECRET
  - TWITTER_ACCESS_TOKEN
  - TWITTER_ACCESS_SECRET
  
6. Edit the configuration values :

  * General
    - *CAN_POST* : if false, the script will not run ("maintenance mode")
    - *MAINTENANCE* : same as previous value
    - *FOLDER* : data folder, currently the folder where `replace.txt` is located (don't forget the trailing slash)
    - *IMAGES_FOLDER* (deprecated) : folder containing images from which the script will choose one to post on twitter
    - *IS_DEBUG* : will not post on twitter if true
    - *RULES_FILE* : file containing rules to post images when defined conditions are met (date/time...)
    - *MAX_RETRIES* : number of times the script should try to find a picture before failing
  * Plugins
    - *PLUGINS_FOLDER* : location of the files downloaded at step 4
    - *PLUGIN* : Name of the plugin to use to download files (possible values : local, ftp, dropbox)
  * WS (push server related values, see listener section)
    - *active* : is the server active ? 
    - *server_url* : complete url to the server (default ws://127.0.0.1:8000)
  * Twitter
    - *TWITTER_CREDS* : path to the file containing twitter credentials (step 5)
    - *TWITTER_SIZE_LIMIT* : maximum image size, files with a size above this value will not be posted
  * Pushbullet
    - *PB_ACTIVE* : enable/disable pushbullet notifications (send a message to the defined device when post failed)
    - *PB_CREDS* : pushbullet credentials to use

7. (optional) set up a cron/scheduled task to automatically run the script every X minutes

# Pushbullet

//TODO

# Write your own plugins

//TODO
