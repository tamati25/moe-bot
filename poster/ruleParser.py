#!/usr/bin/env python

import time
import json
import datetime
import random
from pprint import pprint
import ConfigParser
import os

cfg = ConfigParser.ConfigParser()
cfg.read(os.path.dirname(os.path.realpath(__file__)) + os.sep + "ruleParser.py.config")

logpath = cfg.get("general", "logpath")

logfile = []
if os.path.exists(logpath):
  logfile = [line.strip() for line in open(logpath)]

# -------------
# Logger
# -------------
def log(text):
  print '[', str(datetime.datetime.now()), ']', text

# -------------
# Basic checks
# -------------
def check_day(day):
    log("    [COND] day = " + str(day))
    today = datetime.datetime.now()
    log("           day = " + str(today.day))
    return (today.day == day)

def check_month(month):
    log("    [COND] month = " + str(month))
    today = datetime.datetime.now()
    log("           month = " + str(today.month))
    return (today.month == month)

def check_year(year):
    log("    [COND] year = " + str(year))
    today = datetime.datetime.now()
    log("           year = " + str(today.year))
    return (today.year == year)

def check_weekday(weekday):
    log("    [COND] weekday = " + str(weekday))
    today = datetime.datetime.now()
    log("           weekday = " + str(today.weekday))
    return (today.weekday == weekday)

def check_hour(hour):
    log("   [COND] hour = " + str(hour))
    today = datetime.datetime.now()
    return (today.hour == hour)

def random100(threshold):
    log("  [COND] random100 < " + str(threshold) + ")")
    result = random.randint(1, 100) - 1
    log("         got " + str(result))
    return (result < threshold)

def randomN(threshold, maxN):
    log("  [COND] random(" + str(maxN) + " < " + str(threshold) + ")")
    result = random.randint(1, maxN) - 1
    log("         got " + str(result))
    return (result < threshold)

def hour_between(minH, maxH):
    log("  [COND] " + str(minH) + " <= hour <= " + str(maxH))
    today = datetime.datetime.now()
    return (minH <= today.hour and today.hour <= maxH)
  

# -------------
# json parsing
# -------------
def check_cond(cond):
    result = False

    if cond.has_key("CondAnd"):
      result = check_cond_and(cond["CondAnd"])
    elif cond.has_key("CondOr"):
      result = check_cond_or(cond["CondOr"])
    elif cond.has_key("Day"):
      result = check_day(cond["Day"])
    elif cond.has_key("Month"):
      result = check_month(cond["Month"])
    elif cond.has_key("Weekday"):
      result = check_weekday(cond["Weekday"])
    elif cond.has_key("Hour"):
      result = check_hour(cond["Hour"])
    elif cond.has_key("Random100"):
      result = random100(cond["Random100"])
    elif cond.has_key("RandomN"):
      result = randomN(cond["RandomN"]["threshold"], cond["RandomN"]["max"])
    elif cond.has_key("HourBetween"):
      result = hour_between(cond["HourBetween"]["min"], cond["HourBetween"]["max"])
    # TODO complete list
    else:
        raise Exception("Unknown condition : ", cond)

    log("Check result : " + str(result))
    return result

def check_cond_or(conds):
    log("  [COND OR]")
    result = False
    for cond in conds:
        result = result or check_cond(cond)
        if result is True:
            return True
    return result

def check_cond_and(conds):
    log("  [COND AND]")
    result = True
    for cond in conds:
        result = result and check_cond(cond)
        if result is False:
            return False
    return result

def check_cond_nand(conds):
    log("  [COND NAND]")
    result = True
    for cond in conds:
      result = result and not check_cond(cond)
      if result is False:
        return False
    return result

def check_cond_nor(conds):
    log("  [COND NOR]")
    result = False
    for cond in conds:
      result = result or not check_cond(cond)
      if result is True:
        return True
    return result

def check_rule(rule):
    log("Checking rule " + rule["Name"])

    if rule.has_key("Execute") and quota_reached(rule["Name"], rule["Execute"]):
      log("Quota of " + str(rule["Execute"]) + " reached for today, skipping rule.")
      return False

    if rule.has_key("CondAnd"):
        return check_cond_and(rule["CondAnd"])
    elif rule.has_key("CondOr"):
        return check_cond_or(rule["CondOr"])
    elif rule.has_key("CondNand"):
        return check_cond_nand(rule["CondNand"])
    elif rule.has_key("CondNor"):
        return check_cond_nor(rule["CondNor"])
    elif rule.has_key("Always"):
        return True
    else:
        raise Exception("Condition missing, need 'CondAnd' or 'CondOr'")

# -------------
# Check the number of executions today (reset every day at 12:00am)
# -------------
def quota_reached(name, limit):
  log("Checking quota [limit = " + str(limit) + "]")
  count = 0
  for line in logfile:
    if line == name:
      count = count + 1
  log("Today execution count : " + str(count))
  return count >= limit

# -------------
# Process rule
# -------------
def process_rule(rule):
    log("Conditions verified, processing rule")
    imgSet = rule["Img"]
    setLength = len(imgSet)
    r = random.randint(1, setLength) - 1
    log("Chose : " + imgSet[r])
    return imgSet[r]

def check_valid_json(data_file):
  log("Checking file validity")
  try:
    data = json.load(data_file)
    log("Valid")
    return True
  except Exception, e:
    log(str(e))
    return False

# -----------------
# Main entry point
# -----------------
def parse(file):
  log("Processing rule file " + file)
  with open(file) as data_file:
    if check_valid_json(data_file) is False:
      log("Invalid rule file, rules will not be checked")
      return None

  #TODO : avoid loading the file twice...
  with open(file) as data_file:
    data = json.load(data_file)
    result = False

    #TODO : pre

    log("Processing rules")
    for rule in data["Rules"]["Rules"]:
      result = result or check_rule(rule)
      if result is True:
        imgToPost = process_rule(rule)
        with open(logpath, "a+") as f:
          f.write(rule["Name"] + "\n")
          
          #TODO : post
          return imgToPost
          break

    #TODO : post
    log('No rule processed')
    return None

if __name__ == '__main__':
    parse('moe_poster_rules.json')
