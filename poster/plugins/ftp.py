#!/usr/bin/env python

import plugins.plugin as p
from plugins.plugin import Plugin

import tempfile
from ftplib import FTP

class Ftp(Plugin):
    name = "ftp";

    def __init__(self):
        super(self.__class__, self).__init__()
        pass

    def getName(self):
        return Ftp.name

    def init(self, source, **kwargs):
        self.source = source

        self.host = source.split(':')[0]
        if len(source.split(':')) > 1:
            self.port = source.split(':')[1].split('/')[0]

        # Default values
        self.folder = None
        self.login = None
        self.password = None

        if len(source.split('/')) > 1:
            self.folder = ('/').join(source.split('/')[1:])
        if 'login' in kwargs:
            self.login = kwargs['login']
        if 'password' in kwargs:
            self.password = kwargs['password']
        print self.login, " / ", self.password, " : ", self.folder

        self.ftp = FTP()
        self.ftp.connect(self.host, self.port)
        if self.login is not None or self.password is not None:
            self.ftp.login(self.login, self.password)
        if self.folder is not None:
            self.ftp.cwd(self.folder)

    def get_list(self):
        return self.ftp.nlst()

    def download(self, filename):
        result = {}
        ext = "." + filename.split(".")[-1]
        self.localfile = tempfile.mkstemp(suffix=ext)
        print self.localfile
        self.localhndl = open(self.localfile[1], "wb")
        try:
            self.ftp.retrbinary("RETR " + filename, self.localhndl.write)
            result["status"] = True
            result["filename"] = self.localfile[1]
            result["title"] = filename
        except Exception as e:
            print "Error while downloading remote file"
            print str(e)
            result["status"] = False
        return result

    def close(self):
        if not self.localhndl.closed:
            self.localhndl.close()
