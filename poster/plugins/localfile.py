#!/usr/bin/env python

import plugins.plugin as p
from plugins.plugin import Plugin

import os

class Local(Plugin):
    name = "local";

    def __init__(self):
        super(self.__class__, self).__init__()
        pass

    def getName(self):
        return Local.name

    def init(self, source, **kwargs):
        self.source = source

    def get_list(self):
        self.fileslist = os.listdir(self.source)
        return self.fileslist

    def download(self, filename):
        result = {}
        result["status"] = True
        result["title"] = filename
        result["filename"] = self.source + filename
        return result

    def search(self, keyword):
        for i in self.fileslist:
            filename = ''.join(keyword.split('.')[:-1])
            if (i == filename):
                return i
        return None

    def close(self):
        pass

