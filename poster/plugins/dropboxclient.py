#!/usr/bin/env python

import plugins.plugin as p
from plugins.plugin import Plugin

import os
import dropbox
import tempfile

class DropboxClient(Plugin):
    name = "dropbox";

    def __init__(self):
        super(self.__class__, self).__init__()
        pass

    def getName(self):
        return DropboxClient.name

    def init(self, source, **kwargs):
        self.auth = kwargs["authfile"]
        self.folder = source

        token = [line.strip() for line in open(self.auth)][0]
        self.client = dropbox.client.DropboxClient(token)

    def get_list(self):
        result = []
        self.files = self.client.metadata(self.folder)
        n_of_files = len(self.files["contents"])
        for i in range(0, n_of_files):
            result.append(os.path.basename(self.files["contents"][i]["path"]))
        return result

    def download(self, filename):
        result = {}
        ext = "." + filename.split('.')[-1]
        self.localfile = tempfile.mkstemp(suffix=ext)
        self.localhndl = open(self.localfile[1], "wb")
        try:
            # Download file
            with self.client.get_file(self.folder + "/" + filename) as f:
                self.localhndl.write(f.read())

            result["status"] = True
            # Give an arbitrary extension allowed by twitter
            result["filename"] = self.localfile[1]
            result["title"] = filename
        except Exception as e:
            print "Error while downloading remote file"
            print str(e)
            result["status"] = False
        return result

    def close(self):
        if not self.localhndl.closed:
            self.localhndl.close()
