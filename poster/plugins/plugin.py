#!/usr/bin/env python

class Plugin(object):
    # Plugin name
    name = 'Interface'

    def __init__(self):
        self.clients = []

    def getName(self):
        return self.name

    def isDebug(self):
        return False

    # Initialize the plugin (open connection, initialize variables ...)
    def init(self, source, **kwargs):
        pass

    # Get a list of the pictures available
    # returns a list of strings
    def get_list(self):
        raise Exception('Subclasses must implement get_list method')

    # Logic to download a picture to the file passed as parameter
    def download(self, filename):
        raise Exception('Subclasses must implement download method')

    # Close the connection
    def close(self):
        pass

    # Search for an image, returns a list of candidates
    def search(self, keyword):
        pass
