#!/usr/bin/env python

from StringIO import StringIO

import datetime
import signal

from tweepy import StreamListener
from tweepy import Stream
import tweepy

import Queue
import threading
import pprint, json

import ConfigParser

import sys
import moe_command

#Yay, hard coded keys~
cfg = ConfigParser.ConfigParser()
cfg.read("listener.py.config")
twitter_keys = [line.strip() for line in open(cfg.get("twitter", "auth_file"))]

CONSUMER_KEY = twitter_keys[0]
CONSUMER_SECRET = twitter_keys[1]
ACCESS_KEY = twitter_keys[2]
ACCESS_SECRET = twitter_keys[3]

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

class StdOutInterceptor():
    def __init__(self, oldstdout):
        self.old_stdout = oldstdout

    def write(str):
        str = '[' + str(datetime.datetime) + ']' + str
        self.old_stdout.write(str)

queue = Queue.Queue()
class StdOutListener(StreamListener):

    def on_status(self, data):
        # process stream data here
        print('\033[1;32mData :\033[0m')

        print 'Parsing data (moe_command)'
        parse_data = threading.Thread(target = moe_command.parse, args=[data, queue])
        parse_data.start()
        parse_data.join()
        return_val = queue.get()
        print "Parse result : ", return_val
        if return_val is not None:
            api.send_direct_message(screen_name=data.user.screen_name, text=return_val)
        print 'Ready'

#        moe_command.parse(data)
#        print data
#        pprint.pprint(json.loads(data))
        return True

    def on_error(self, status):
        print(status)

    def on_event(self, status):
        print('\033[1;32mEvent :\033[0m')
#        moe_command.parse(status.Status)
#        pprint.pprint(status._json)
#        print(status)
        return True

def sigterm_handler(signo, stack_frame):
    print 'SIGTERM received'
    sys.stdout = old_stdout

if __name__ == '__main__':
    signal.signal(signal.SIGTERM, sigterm_handler)
    old_stdout = sys.stdout
    stdout = StdOutInterceptor(old_stdout)
    listener = StdOutListener()
    twitterStream = Stream(auth, listener)
    print 'Ready to listen'
    twitterStream.userstream()
