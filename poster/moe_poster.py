#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""
Moe poster : Posts one image from an image folder on twitter

TODO:
"""

__author__ = "/u/tama_92"

import os
import dropbox
import tweepy
import urllib2
import random
import magic
from datetime import datetime
import sys
import re
import traceback
import HTMLParser
import ConfigParser
import difflib
import websocket
import loader

# Read configuration file
configParser = ConfigParser.RawConfigParser()
configFilePath = os.path.dirname(os.path.realpath(__file__)) + os.sep + 'moe_poster.py.config'
configParser.read(configFilePath)

sys.path.append(configParser.get('request', 'request_script_path'))
import request

# parse a json file containing rules
import ruleParser

# force UTF-8
reload(sys)
sys.setdefaultencoding("utf-8")

def log(text):
  print '[', str(datetime.now()), ']', text

if configParser.get('general', 'CAN_POST') == 'false' or configParser.get('general', 'MAINTENANCE') == 'true':
  log('Posting disabled in configuration file')
  sys.exit()

# Path to the files containing rules ; this file contains rules which will enable the bot to post specific
# pictures when conditions are met (day/time ...)
rules_file = configParser.get('general', 'RULES_FILE')

# environment variables from the configuration file
folder = configParser.get('general', 'FOLDER')
max_retries = int(configParser.get('general', 'MAX_RETRIES'))

# Pushbullet : if active, will send a notification to a device when posting to twitter failed.
# see https://www.pushbullet.com/api for details on how to set it up
pushbullet_active = configParser.getboolean("pushbullet", "active")
if pushbullet_active is True:
  from pushbullet import PushBullet

# Defines which plugin the poster should use, default one is "local" (get one picture from a local folder)
plugins_folder = "plugins"
plugin_name = "local"
try:
  plugins_folder = configParser.get('plugins', 'PLUGINS_FOLDER')
  plugin_name = configParser.get('plugins', 'PLUGIN')
except Exception:
  print "No plugin defined, the default one [local] will be used"

# twitter access token
twitter_credentials = [line.strip() for line in open(configParser.get('twitter', 'TWITTER_CREDS'))]
API_KEY = twitter_credentials[0]
API_SECRET = twitter_credentials[1]
CONSUMER_KEY = twitter_credentials[2]
CONSUMER_SECRET = twitter_credentials[3]
FILE_SIZE_LIMIT = int(configParser.get('twitter', 'TWITTER_SIZE_LIMIT'))

if pushbullet_active is True:
  pushbullet_credentials = [line.strip() for line in open(configParser.get('pushbullet', 'PB_CREDS'))]
  PB_TOKEN = pushbullet_credentials[0]
  pb = PushBullet(PB_TOKEN)

random_index = 0
debug = configParser.getboolean('general', 'IS_DEBUG') is True

# Initialize plugin
plugins = loader.get_plugins(plugins_folder)
plugin = plugins[plugin_name]()

try:  
  if debug is True:
    log('***** DEBUG MODE *****')
  else:
    log('***** PRODUCTION MODE *****')

  log('------')
  log('Starting')
  log('------')

  extra_args = dict(x.split('=', 1) for x in sys.argv[2:])
  plugin.init(sys.argv[1], **extra_args)
  
  is_found = False
  log('Listing files')
  files = plugin.get_list()
  n_of_files = len(files)

  localfilename = ''
  n_of_tries = 0
  random_index = 0
  result = {}

  #TODO : REFACTORING !!!
  is_request = False

  filename = request.check()
  if filename is not None:
    is_request = True

  if is_request is False:
    rule_file = ruleParser.parse(rules_file)

    if rule_file is None:
      # No rule applied, choose a random file
      while is_found == False and n_of_tries < max_retries:
        n_of_tries = n_of_tries + 1
        random_index = random.randint(1,n_of_files) - 1
        result = plugin.download(files[random_index])
        is_found = result["status"]
        if is_found is True:
          result["index"] = random_index
    else:
      # build an object
      result = plugin.download(plugin.search(rule_file))

    if n_of_tries >= max_retries:
      log("Max retries number reached.")
      sys.exit(0)
  else:
    result = plugin.download(plugin.search(rule_file))

  if result["status"] is False:
    log('Failed to find a file to post. Exiting')
    sys.exit()

  title = result["title"]
  filename = result["filename"].encode('utf-8', 'ignore')
  random_index = result["index"]

  #found one, print file name
  log('Posting "' + filename + '" to twitter')
  log('Connecting to twitter')
  auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
  auth.set_access_token(CONSUMER_KEY, CONSUMER_SECRET)
  api = tweepy.API(auth)

  log('Posting...')
  filenameWithoutExt = ('').join(title.split(".")[:-1])

  if is_request is False:
    twitter_status = '[Moe bot][#' + str(random_index) + '] ' + filenameWithoutExt
  else:
    twitter_status = '[Moe bot][REQUEST] ' + filenameWithoutExt

  # Do some processing on the text (TODO : put in a separate function)
  # truncate to 100 characters max
  if len(twitter_status) > 97:
    twitter_status = twitter_status[:97] + "..."

  # replace with the dictionary defined in "replace.txt" file
  replace_file_location = folder + "replace.txt"
  with open(replace_file_location.encode('utf-8')) as f:
    for line in f:
      if not line.startswith("#"):
        split = line.split()
        word_to_replace = ' '.join(split[:-1])
        replacement = split[-1:]
        if len(replacement) > 0:
          pattern = re.compile(re.escape(word_to_replace), re.IGNORECASE)
          twitter_status = re.sub(pattern, replacement[0], twitter_status)
          
          #Fix the consecutive # (FIXME : find a better way...)
          pattern = re.compile("#{2,}")
          twitter_status = re.sub(pattern, "#", twitter_status)

  h = HTMLParser.HTMLParser()
  status = HTMLParser.HTMLParser().unescape(twitter_status)

  if debug is False:
    log("Post => " + status + " (file : " + filename + ")")
    api.update_with_media(filename, status)
    log('Posted : ' + status + ' with picture ' + filename)
  else:
    log('[DEBUG MODE] Status : ' + status + ', file path : ' + filename)
    log('[DEBUG MODE] not posting.')

  # Update requests
  if configParser.getboolean("ws", "active") is True:
    log("Pushing refresh message to server")
    try:
      ws = websocket.create_connection(configParser.get("ws", "server_url"))
      msg = ws.recv()
      sid = msg.split("/")[1]

      ws.send(sid + "/moe/connect")
      ws.send(sid + "/moe/refresh")
      ws.close()
      log("Pushed.")
    except:
      log('Failed :(')
      pass

  log('Cleaning...')
  plugin.close()
except Exception, e:
  tb = traceback.format_exc()

  if debug is False:
    if pushbullet_active is True:
      device = pb.devices[0]
      success, push = device.push_note("[moe poster][#" + str(random_index) + "] Error occured during post", tb)
  else:
    # "DEV" : print a message on terminal
    log('[moe poster][#' + str(random_index) + '] Error occured during post')

  log('Error : ' + tb)

log('------')
log('Finished')
log('------')  
