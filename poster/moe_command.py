#!/usr/bin/env python
#-*- coding: utf-8 -*-

import difflib
import os
import re
import dropbox
import tweepy
from datetime import datetime
import time
import sys
import unicodedata
import subprocess
import websocket
import ConfigParser

# force UTF-8
reload(sys)
sys.setdefaultencoding("utf-8")

cfg = ConfigParser.ConfigParser()
cfg.read(os.path.dirname(os.path.realpath(__file__)) + os.sep + "moe_command.py.config")
sys.path.insert(0, cfg.get("request", "request_script_path"))
import request as Request

# Constants
NUMBER_OF_MENTIONS = 50

# Twitter related configuration
NUMBER_OF_MENTIONS = cfg.get("twitter", "n_of_mentions")
auth_file = cfg.get("twitter", "auth_file")
twitter_credentials = [line.strip() for line in open(auth_file)]

data_folder = cfg.get("general", "data_folder")
images_folder = cfg.get("general", "images_folder")
min_match_ratio = cfg.getfloat("general", "min_match_ratio")
create_link_script = cfg.get("general", "create_link_script")

request_file = cfg.get("request", "request_file")
new_request_added = False

ws_active = cfg.getboolean("ws", "active")
ws_server_url = cfg.get("ws", "server_url")

def notify_clients():
  if ws_active is False:
    return

  log("Pushing refresh message to server")
  try:
    ws = websocket.create_connection(ws_server_url)
    msg = ws.recv()
    sid = msg.split("/")[1]

    ws.send(sid + "/moe/connect")
    ws.send(sid + "/moe/refresh")
    ws.close()
    log("Pushed.")
  except:
    log('Failed :(')
    pass

def log(string):
  print str(datetime.now()), string

def log_error(string):
  print '\033[1;31m', str(datetime.now()), string, '\033[0m'

# !link command handler
def link(tweet, user):
  # Get a link to a picture in a tweet
  file = search(tweet)
  print file
  if file is not None:
    # Launch the script to create a temporary link, the return of the script is the link
    link = subprocess.check_output([create_link_script, images_folder, file])
    return "Link to the picture : " + str(link) + " -- This link will expire in 12 hours"
 
def process(raw_filename, tweet):
  out = raw_filename

  # truncate to 97 chars
  if len(out) > 97:
    out = out[:97]

  # see moe_poster.py
  if os.path.exists(data_folder + "replace.txt"):
    with open(data_folder + "replace.txt") as f:
      for line in f:
        if not line.startswith("#"):
          split = line.split()
          word_to_replace = ' '.join(split[:-1])
          replacement = split[-1:]
          if len(replacement) > 0:
            pattern = re.compile(re.escape(word_to_replace), re.IGNORECASE)
            out = re.sub(pattern, replacement[0], out)

            pattern = re.compile("#{2,}")
            out = re.sub(pattern, "#", out)

            # remove the last character until the strings have the same length
            for i in range(len(out), len(tweet), -1):
              out = out[:len(out) - 1]
    
  return out

# Use dropbox api to get a link to the file from the title given
# Will return a file at least min_match_ratio (default 95%) similar or None if not found
# If an exact match is found, will return directly after it
def search(tweet):

  # remove all the extra informations from the tweet status
  tweet = re.sub('\[Moe bot\][\#[0-9]+\] ', '', tweet)
  tweet = re.sub('\.\.\. ', '', tweet)
  tweet = re.sub('http://t.co.*$', '', tweet)

  out = None
  files = os.listdir(images_folder)
  n_of_files = len(files)

  most_similar = 0
  most_similar_filename = None

  tweet = tweet.decode('utf8', errors = 'ignore')

  for i in range(0, n_of_files):
    dbfile = files[i]
    filename = ('').join(dbfile.split(".")[:-1])

    #see moe poster, the name is truncated to get only 100 chars in the tweet
    twitter_filename = process(filename, tweet).decode('utf8', errors = 'ignore')

    # exact match found, no need to continue
    if twitter_filename == tweet:
      log("  [EXACT MATCH]" + str(i) + " > " +filename)
      return filename

    similarity_ratio = difflib.SequenceMatcher(None, twitter_filename, tweet).ratio()

    #log("  " + str(i) + " > " + twitter_filename + " / " + tweet + "[" + str(similarity_ratio) + "]")
    if similarity_ratio >= min_match_ratio and similarity_ratio >= most_similar:
      log(str(i) + " > " + twitter_filename + " / " + tweet + "[" + str(similarity_ratio) + "]")
      most_similar_filename = dbfile
      most_similar = similarity_ratio

  # return the one which "matches the most"
  print most_similar_filename
  return most_similar_filename

def add_request(username, request):
  #TODO : check if there is a picture for this request, don't add it if not (but still send a DM to the user to inform him/her)
  if os.geteuid() == 0:
    check_exists = Request.check_exists(request)
    if check_exists is False:
      return False

  with open(request_file, "a") as f:
    f.write(str(int(time.time())) + ":" + username + ":" + request + "\n")
    return True

# Dispatcher
def parse(status, q = None):
  screen_name = status.user.screen_name
  text = status.text
  output = None

  words = text.split()
  if (len(words) > 2):
    command = words[1]
    if words[1] == '!fullsize':
      output = "the fullsize command is deprecated, reply 'link' to the tweet to get the download link"
    elif words[1] == '!more' or words[1] == "!More": 
      is_ok = add_request(screen_name, " ".join(words[2:]))
      if is_ok is True:
        new_request_added = True
        notify_clients()
        output = "Request accepted, it will be added to the list if the rules are respected (see http://moe-moe.ovh/request)"
      else:
        output = "The request was received but no picture was found, so it was not saved, see checker tool at http://moe-moe.ovh/request to check your requests"

  if output is None:
    in_reply_to = status.in_reply_to_status_id

    reply_log_txt = ''
    if in_reply_to is not None and 'link' in text:
      parent_tweet = api.get_status(in_reply_to)
      log('  In reply to : [' + str(in_reply_to) + '] @tamamoebot : ' + parent_tweet.text)
      output = link(parent_tweet.text, screen_name)

  if q is not None:
    print 'Storing in queue : ', output
    q.put(output)

  print 'Parse End => ', output
  return output
  #return 'Not working now, please try again later >.<'

if __name__ == '__main__':
  # Main entry point

  API_KEY = twitter_credentials[0]
  API_SECRET = twitter_credentials[1]
  CONSUMER_KEY = twitter_credentials[2]
  CONSUMER_SECRET = twitter_credentials[3]
  auth = tweepy.OAuthHandler(API_KEY, API_SECRET)
  auth.set_access_token(CONSUMER_KEY, CONSUMER_SECRET)
  api = tweepy.API(auth)

  log('-------')
  log('Started at ' + str(datetime.now()))
  log('-------')

  log('Getting (50) last mentions')
  mentions_file = open(data_folder + "mentions", "ab+")
  mentions_id = [line.strip() for line in mentions_file]

  mentions = api.mentions_timeline(count=NUMBER_OF_MENTIONS)
  for mention in mentions:
    if str(mention.id) not in mentions_id:
      mentions_file.write(str(mention.id) + "\n")
      log('[' + str(mention.id) + '] ' + '@' + str(mention.user.screen_name) + ' : ' + mention.text)
      reply = parse(mention)
      if reply is not None:
        try:
          log("  [DM] -> @" + mention.user.screen_name + " " + reply)
          api.send_direct_message(screen_name=mention.user.screen_name, text=reply)
        except Exception as e:
          log('  Failed to send private message, will retry later.')

      if new_request_added is True:
        notify_clients()

  mentions_file.close()

  log('-------')
  log('Ended at ' + str(datetime.now()))
  log('-------')
